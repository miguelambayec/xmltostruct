package main

import (
  "fmt"
  "encoding/xml"
)

type Response struct {
  Header Header
  Body Body
}

type Body struct {
  Officematessss Officemates
}

type Officemates struct {
  Employee []string
}

type Header struct {
  Credentials Credentials
}

type Credentials struct {
  UserName string `xml:",attr"`
  Password string `xml:",attr"`
}

func main() {
  xmlString := `
  <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:hot="http://TekTravel/HotelBookingApi">
  <soap:Header xmlns:wsa='http://www.w3.org/2005/08/addressing' >
  <hot:Credentials UserName="testuser" Password="testpwd">
  </hot:Credentials>
  <wsa:Action>http://TekTravel/HotelBookingApi/CountryList</wsa:Action>
  <wsa:To>http://api.tbotechnology.in/hotelapi_v7/hotelservice.svc</wsa:To>
  </soap:Header>
  <soap:Body>
    <hot:Officemates>
      <hot:Employee>Migz</hot:Employee>
      <hot:Employee>Myck</hot:Employee>
    </hot:Officemates>
  </soap:Body>
  </soap:Envelope>
  `

  interfaceResponse := Response{}

  err := xml.Unmarshal([]byte(xmlString), &interfaceResponse)
  if err != nil {
    fmt.Println(err)
  }

  fmt.Printf("%#v\n", interfaceResponse)

}
