package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"training/xmlToStruct-original/model"
)

var StructHolder []model.Struct
var Commands []model.Command
var notag, writetofile bool
var output string

func main() {

	initializeCommands()

	// GET XML STRING FROM OTHER FILE
	xmlString := getXmlStringAndCommands()

	// RECURSION OF CREATION OF STRUCT
	createStruct(xmlString, "MAIN", []model.Attribute{})

	// DISPLAYS THE STRUCT ON THE
	buildOutput()

	if writetofile {
		writeToFile("xml_to_struct_output.go")
		fmt.Println("Successfully generated a file containing the struct for the xml")
	} else {
		fmt.Println("\n" + output)
	}

}

func initializeCommands() {
	Commands = append(Commands, model.Command{
		Name:   "-nt",
		Status: false,
	})

	Commands = append(Commands, model.Command{
		Name:   "-wo",
		Status: false,
	})
}

func removePrefix(name string) string {

	if strings.Contains(name, ":") {
		return strings.Split(name, ":")[1]
	}

	return name
}

func writeToFile(path string) {

	// write the whole body at once
	err := ioutil.WriteFile(path, []byte(output), 0644)
	if err != nil {
		panic(err)
	}
}

func getXmlStringAndCommands() string {
	b, err := ioutil.ReadFile(os.Args[len(os.Args)-1])
	if err != nil {
		fmt.Print(err)
	}

	// Detect commands if called
	for key, Command := range Commands {

		for i := 1; i <= len(os.Args)-1; i++ {

			if os.Args[i] == Command.Name {

				Commands[key].Status = true
			}
		}
	}

	notag = Commands[0].Status
	writetofile = Commands[1].Status

	return string(b)
}

func createStruct(str string, structName string, structAttrs []model.Attribute) {

	// CREATION OF STRUCT TO BE ADDED IN THE HOLDER

	Struct := model.Struct{}
	Struct.StructName = formatName(structName)

	if !notag {
		structAttrs = append(structAttrs, model.Attribute{
			Name:     "XMLName",
			DataType: "xml.Name",
			Tag:      `xml:"` + structName + `"`,
		})
	}
	Struct.Attributes.Attribute = structAttrs

	// IF STRINGS STILL HAS STRUCT VALUE INSIDE
	for hasStructValue(str) {

		var re = regexp.MustCompile(`<([^<]*)>`)

		// GET PARENT TAG
		rawParent := re.FindStringSubmatch(str)[1]

		// GET ATTR OF RAW PARENT
		separatedParent, attrs := getAttrs(rawParent)

		// GET PARENT ATTRIBUTES
		re = regexp.MustCompile("<" + rawParent + `>([\s\S]*?)</` + separatedParent + ">")

		content := re.FindAllStringSubmatch(str, -1)

		// IF SELF CLOSING TAG
		if content == nil {
			re = regexp.MustCompile("<" + rawParent + ">")
			content = re.FindAllStringSubmatch(str, -1)

			// OPTIONAL
			if content == nil {
				log.Println("This is nil <" + rawParent + ">")
			}

			innerStringValue := content[0][0]

			// SET ATTRIBUTE
			tmpAttribute := model.Attribute{}
			tmpAttribute.Name = formatName(separatedParent)

			if !notag {
				tmpAttribute.Tag = `xml:"` + separatedParent + `"`
			}

			tmpAttribute.DataType = tmpAttribute.Name

			// ADD THE ATTRIBUTE TO THE STRUCT
			Struct.Attributes.Attribute = append(Struct.Attributes.Attribute, tmpAttribute)

			// DELETE STRUCT ATTRIBUTE FROM THE STRING
			str = strings.Replace(str, innerStringValue, "", -1)

			/* STRUCT CREATION FOR SELF CLOSING ATTRIBUTE */

			// CREATE STRUCT FOR SELF CLOSING
			selfClosingStruct := model.Struct{}
			selfClosingStruct.StructName = formatName(separatedParent)

			if !notag {
				attrs = append(attrs, model.Attribute{
					Name:     "XMLName",
					DataType: "xml.Name",
					Tag:      `xml:"` + separatedParent + `"`,
				})
			}

			selfClosingStruct.Attributes.Attribute = attrs

			// ADD STRUCT TO HOLDER
			if !structExists(selfClosingStruct) {
				StructHolder = append(StructHolder, selfClosingStruct)
			}

			/* END OF CREATION */

		} else {

			innerStringValue := content[0][1]

			// SET ATTRIBUTE
			tmpAttribute := model.Attribute{}
			tmpAttribute.Name = formatName(separatedParent)

			if !notag {
				tmpAttribute.Tag = `xml:"` + separatedParent + `"`
			}

			if hasStructValue(innerStringValue) || hasAttrs(rawParent) {
				tmpAttribute.DataType = tmpAttribute.Name
				createStruct(innerStringValue, separatedParent, attrs)
			} else {
				tmpAttribute.DataType = detectDataType(string(innerStringValue))
			}

			// ADD THE ATTRIBUTE TO THE STRUCT
			Struct.Attributes.Attribute = append(Struct.Attributes.Attribute, tmpAttribute)

			// DELETE STRUCT ATTRIBUTE FROM THE STRING
			str = strings.Replace(str, content[0][0], "", -1)

		}

	}

	// ADD STRUCT TO HOLDER
	if !structExists(Struct) {
		StructHolder = append(StructHolder, Struct)
	}

}

// CHECKS IF THE RAW PARENT HAS ATTRS
func hasAttrs(rawParent string) bool {
	return strings.Contains(rawParent, "=")
}

// REMOVES SPECIAL CHARACTERS FROM A STRING
// THIS IS USED FOR STRUCT NAMES AND ATTRIBUTE NAMES
func formatName(name string) string {
	name = removePrefix(name)
	name = strings.Title(name)
	re := regexp.MustCompile("[$&+,:;=?@#|'<>.^*()%!-/]")
	specialCharacters := re.FindAllStringSubmatch(name, -1)

	for _, character := range specialCharacters {

		name = strings.Replace(name, character[0], "", -1)
	}
	return name
}

// USED FOR DISPLAYING ALL THE STRUCTS IN THE TERMINAL
func buildOutput() {

	output += "package IKAW_NA_BAHALA_DITO_AKO_NA_NGA_NAG_GENERATE_EH_TSK_TAMAD_EH_XD\n\n"

	if !notag {
		output += `import "encoding/xml"` + "\n\n"
	}

	for i := len(StructHolder) - 2; i >= 0; i-- {

		// fmt.Println("type " + StructHolder[i].StructName + " struct {")
		output += "type " + StructHolder[i].StructName + " struct {\n"

		// DETECTS ARRAY ATTRIBUTE
		// IF ARRAY, ATTRIBUTE WILL BE DISPLAYED AS []<Attribute Name>
		detectArrayAttribute(StructHolder[i].Attributes.Attribute)

		// fmt.Print("}\n\n")
		output += "}\n\n"

	}
}

// DETECTS ARRAY ATTRIBUTE
// IF ARRAY, ATTRIBUTE WILL BE DISPLAYED AS []<Attribute Name>
func detectArrayAttribute(Attributes []model.Attribute) {

	// GET THE COUNT OF ATTRIBUTE
	AttributeCount := make(map[string]int)

	for _, Attribute := range Attributes {

		count := 0
		for _, attr := range Attributes {

			if Attribute.Name == attr.Name {
				count++
			}
		}
		AttributeCount[Attribute.Name] = count
	}

	// DISPLAY XMLNAME AS THE FIRST ATTRIBUTE
	for attrName, count := range AttributeCount {

		for _, Attribute := range Attributes {

			if attrName == Attribute.Name && attrName == "XMLName" {

				if count > 1 {

					if notag {
						// fmt.Println("\t" + Attribute.Name + " []" + Attribute.DataType)
						output += "\t" + Attribute.Name + " []" + Attribute.DataType + "\n"
					} else {
						// fmt.Println("\t" + Attribute.Name + " []" + Attribute.DataType + " `" + Attribute.Tag + "`")
						output += "\t" + Attribute.Name + " []" + Attribute.DataType + " `" + Attribute.Tag + "`" + "\n"
					}

					break
				} else {

					if notag {
						// fmt.Println("\t" + Attribute.Name + " " + Attribute.DataType)
						output += "\t" + Attribute.Name + " " + Attribute.DataType + "\n"
					} else {
						// fmt.Println("\t" + Attribute.Name + " " + Attribute.DataType + " `" + Attribute.Tag + "`")
						output += "\t" + Attribute.Name + " " + Attribute.DataType + " `" + Attribute.Tag + "`\n"
					}

				}
			}
		}
	}

	// DISPLAY THE REST OF ATTRIBUTES
	for attrName, count := range AttributeCount {

		for _, Attribute := range Attributes {

			if attrName == Attribute.Name && attrName != "XMLName" {

				if count > 1 {

					if notag {
						// fmt.Println("\t" + Attribute.Name + " []" + Attribute.DataType)
						output += "\t" + Attribute.Name + " []" + Attribute.DataType + "\n"
					} else {
						// fmt.Println("\t" + Attribute.Name + " []" + Attribute.DataType + " `" + Attribute.Tag + "`")
						output += "\t" + Attribute.Name + " []" + Attribute.DataType + " `" + Attribute.Tag + "`\n"
					}

					break
				} else {

					if notag {
						// fmt.Println("\t" + Attribute.Name + " " + Attribute.DataType)
						output += "\t" + Attribute.Name + " " + Attribute.DataType + "\n"
					} else {
						// fmt.Println("\t" + Attribute.Name + " " + Attribute.DataType + " `" + Attribute.Tag + "`")
						output += "\t" + Attribute.Name + " " + Attribute.DataType + " `" + Attribute.Tag + "`\n"
					}

				}
			}
		}
	}

}

// GETS THE ATTRS OF THE RAW PARENT
func getAttrs(rawParent string) (string, []model.Attribute) {

	/* STRING ADJUSMENTS */
	// THIS IS DUE TO XML EXTRA SPACES, USAGE OF SINGLE QUOTE INSTEAD OF DOUBLE QUOTES IN ATTRIBUTE VALUES

	// REPLACE ' TO "
	rawParent = strings.Replace(rawParent, "'", `"`, -1)

	// remove tabs and new lines
	rawParent = strings.Replace(rawParent, "\t", " ", -1)
	rawParent = strings.Replace(rawParent, "\n", " ", -1)

	for spacesMorethanOne(rawParent) {
		rawParent = strings.Replace(rawParent, "  ", " ", -1)
	}
	rawParent = strings.Replace(rawParent, `" /`, `"/`, -1)

	// REMOVE " " AT THE END
	if string(rawParent[len(rawParent)-1]) == " " {
		rawParent = rawParent[:len(rawParent)-1]
	}

	/* END OF STRING ADJUSTMENTS */

	// REPLACE SPACES INTO DIFFERENT INSIDE THE VALUES
	// THIS IS DONE BECAUSE I WILL USE SPLIT FUNCTION TO SEPARATE ALL THE ATTRS INTO ARRAY
	re := regexp.MustCompile(`"([\s\S]*?)"`)
	values := re.FindAllStringSubmatch(rawParent, -1)

	// COPY THE VALUES
	var valuesCpy = [][]string{}
	for _, val := range values {
		valuesCpy = append(valuesCpy, []string{
			val[0],
			val[1],
		})
	}

	// REPLACE SPACES TO DIFFERENT CHARACTER
	for key, val := range valuesCpy {
		val[1] = strings.Replace(val[1], " ", "~", -1)
		rawParent = strings.Replace(rawParent, values[key][1], val[1], -1)
	}

	// SEPARATE XMLNAME TO ATTRS
	separatedAttr := strings.Split(rawParent, " ")

	// GET ATTRS
	attributes := []model.Attribute{}
	for i := 1; i < len(separatedAttr); i++ {

		// SET ATTRIBUTE
		tmpAttribute := model.Attribute{}

		// SET ATTR NAME
		attr := strings.Split(separatedAttr[i], "=")
		tmpAttribute.Name = formatName(attr[0])

		if tmpAttribute.Name != "/" {

			// GET ATTR VALUE
			rawAttrValue := attr[1]
			re = regexp.MustCompile(`"([\s\S]*?)"`)
			attrValue := re.FindStringSubmatch(rawAttrValue)[1]
			tmpAttribute.DataType = detectDataType(attrValue)

			// SET ATTR TAG
			if !notag {
				tmpAttribute.Tag = `xml:"` + attr[0] + `,attr"`
			}

			// ADD THE ATTRIBUTE TO THE STRUCT
			attributes = append(attributes, tmpAttribute)
		}

	}

	return separatedAttr[0], attributes
}

// CHECKS IF STRINGS HAS MORE THAN ONE SPACES
// THIS FUNCTION WILL BE USED TO REDUCE ALL THE SPACES MORE THAN ONE
func spacesMorethanOne(str string) bool {
	consecutive := 0
	for i := 0; i < len(str); i++ {

		if string(str[i]) == " " {
			consecutive++

			if consecutive > 1 {
				return true
			}
		} else {
			consecutive = 0
		}
	}
	return false
}

// CHECKS THE STRUCT EXISTS IN THE HOLDER
// USED FOR PREVENTION OF STRUCT REDUNDANCY
func structExists(paramStruct model.Struct) bool {

	for _, Struct := range StructHolder {

		if paramStruct.StructName == Struct.StructName {
			return true
		}
	}

	return false
}

// DETECTS THE DATA TYPE OF A VALUE
func detectDataType(variable string) string {

	// FLOAT
	f, err := strconv.ParseFloat(variable, 64)
	if err == nil {

		if strings.Contains(variable, ".") {
			return reflect.TypeOf(f).String()
		}
	}

	// INT
	i, err := strconv.ParseInt(variable, 10, 64)
	if err == nil {
		return reflect.TypeOf(i).String()
	}

	// BOOL
	b, err := strconv.ParseBool(variable)
	if err == nil {
		return reflect.TypeOf(b).String()
	}

	return "string"

}

// CHECKS IF THE STRING HAS A STRUCT VALUE
func hasStructValue(str string) bool {

	return strings.Contains(str, "<")
}
