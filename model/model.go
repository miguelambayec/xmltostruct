package model

type Struct struct {
	StructName string
	Attributes Attributes
}

type Attributes struct {
	Attribute []Attribute
}

type Attribute struct {
	Name     string
	DataType string
	Tag      string
}

type Command struct {
	Name   string
	Status bool
}
